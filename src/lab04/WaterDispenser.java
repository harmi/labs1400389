/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lab04;

import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Matti
 */
public class WaterDispenser implements Runnable {

    private int maxWaterLevel = 0;
    private int currentWaterLevel = 0;
    private boolean power = false;
    private boolean lidOn = true;
    private boolean valve = false;

    public WaterDispenser(int maxWaterLevel) {
        this.maxWaterLevel = maxWaterLevel;
        this.currentWaterLevel = 10;
        System.out.println("New Water dispenser created!\nMaximum water level at " + this.maxWaterLevel + "ml.\n");
    }

    public void addWater(int amount) {
        if (!this.power) {
            System.out.println("Turn the power on before trying to add water.\n");
        } else if (this.lidOn) {
            System.out.println("Remove the lid before trying to add water.\n");
        } else {
            System.out.println("Adding water...");
            if (this.currentWaterLevel + amount >= this.maxWaterLevel) {
                this.currentWaterLevel = this.maxWaterLevel;
                System.out.println("The tank has now been filled full. \nWater level at " + this.currentWaterLevel + "ml.\n");
            } else {
                this.currentWaterLevel += amount;
                System.out.println("Added " + amount + "ml of water to the tank. \nWater level at " + this.currentWaterLevel + "ml.\n");
            }

        }
    }

    public void togglePower() {
        if (this.power) {
            System.out.println("Turning power off.\n");
            this.power = false;
        } else {
            System.out.println("Turning power on.\n");
            this.power = true;
        }
    }

    public void toggleValve() {
        if (!this.power) {
            System.out.println("Turn on the power before trying toggle the valve.");
        } else if (!this.lidOn) {
            System.out.println("Add the lid before trying to toggle the valve.");
        } else {

            if (this.valve) {
                System.out.println("Valve is in the ON position. Turning it OFF.\n");
                this.valve = false;
            } else {
                System.out.println("Valve is in the OFF position. Turning it ON.\n");
                this.valve = true;
            }
        }
    }

    public void toggleLid() {
        if (!this.power) {
            System.out.println("Turn on the power before opening the lid. It's electronic!");
        } else {
            if (this.lidOn) {
                System.out.println("The lid is CLOSED. Opening it.\n");
                this.lidOn = false;
            } else {
                System.out.println("The lid is OPEN. Closing it.\n");
                this.lidOn = true;
            }
        }
    }

    public void checkWaterLevel() {
        System.out.println("The water level is at " + this.currentWaterLevel);
    }

    @Override
    public void run() {
        while (true) {
            try {
                if (this.currentWaterLevel <= 0) {
                    System.out.println("Not enough water.");
                    break;
                } else if (!this.power) {
                    System.out.println("\nThe power has been turned off!");
                    break;
                } else if (!this.valve) {
                    System.out.println("The valve needs to be in the ON position.");
                    break;
                } else {
                    Thread.sleep(250);
                    System.out.println("*");
                    this.currentWaterLevel -= 1;
                    System.out.println("Water level at " + this.currentWaterLevel);
                }

            } catch (InterruptedException ex) {
                Logger.getLogger(WaterDispenser.class.getName()).log(Level.SEVERE, null, ex);
            }

        }
        System.out.println("Dispensing stopped. Water amount at " + this.currentWaterLevel + ".");

    }

}
