/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lab04;

import java.util.Scanner;

/**
 *
 * @author Matti
 */
public class User {

    public static void main(String[] args) {
        WaterDispenser waterDispenser = new WaterDispenser(1000);
        boolean loop = true;
        int selection = 0;
        Scanner reader = new Scanner(System.in);
        Thread t;

        while (loop) {
            try {
                System.out.println("1. Dispense water\n2. Open/Close valve\n3. Open/Close lid \n4. Add water \n5. Power on/off \n6. Check water level \n7. Exit \n");
                System.out.print("What do you want to do? ");
                selection = Integer.parseInt(reader.nextLine());
                if (selection == 1) {
                    t = new Thread(waterDispenser);
                    t.start();
                    //waterDispenser.run();
                } else if (selection == 2) {
                    waterDispenser.toggleValve();
                } else if (selection == 3) {
                    waterDispenser.toggleLid();
                } else if (selection == 4) {
                    System.out.print("How much water do you want to add? ");
                    selection = Integer.parseInt(reader.nextLine());
                    waterDispenser.addWater(selection);
                } else if (selection == 5) {
                    waterDispenser.togglePower();
                } else if (selection == 7) {
                    System.out.println("Bye bye.");
                    loop = false;
                } else if (selection == 6) {
                    waterDispenser.checkWaterLevel();
                } else {
                    System.out.println("Enter amount between 1-6.");
                }
            } catch (Exception e) {
                System.out.println("Only numbers allowed.");
            }

        }
    }

}
