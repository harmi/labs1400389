/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lab05;

import java.util.Random;

/**
 *
 * @author Matti
 */
public class CD implements Comparable<CD> {

    private String cdName = "";
    private char randomChar;

    public CD() {
        generateCDname();
        //System.out.println("A new CD created: " + this.cdName);
    }

    private void generateCDname() {
        int i = 0;

        while (i<10) {
            i++;
            Random r = new Random();
            this.randomChar = (char) (r.nextInt(26) + 'a');
            this.cdName = this.cdName + this.randomChar;
        }
    }

    @Override
    public int compareTo(CD cd) {
        return (this.cdName).compareTo(cd.cdName);
        
    }

    public void getName() {
        System.out.println(this.cdName);
    }
    
    @Override
    public String toString() {
        return this.cdName;
    }
    
}
