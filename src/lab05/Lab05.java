/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lab05;

import java.lang.management.ThreadMXBean;

/**
 *
 * @author Matti
 */
public class Lab05 {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        int i = 0;
        while (i < 3) {
            i++;
            Controller controller = new Controller(i);
            Thread t = new Thread(controller);
            t.start();
        }
    }
}
