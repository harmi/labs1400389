/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lab05;

/**
 *
 * @author Matti
 */
public abstract class Shelf {
    
    public abstract void addToList(CD cd);
    public abstract void find100th();
    public abstract void arrangeList();
    
}
