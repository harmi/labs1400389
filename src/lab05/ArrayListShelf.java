/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lab05;

import java.util.ArrayList;
import java.util.Collections;

/**
 *
 * @author Matti
 */
public class ArrayListShelf extends Shelf {
    ArrayList<CD> cdList;

    public ArrayListShelf() {
        this.cdList = new ArrayList<>();

    }
    
    @Override
    public void addToList(CD cd) {
        this.cdList.add(cd);
    }
    
    @Override
    public void find100th() {
        for (int i = 0; i < this.cdList.size(); i++) {
            if (i % 10000 == 0) {
                System.out.println("ARRAYLIST Found " + i + "th CD " + this.cdList.get(i).toString());
            }
        }
    }
    
    @Override
    public void arrangeList() {
        Collections.sort(this.cdList);

    }

    
}
