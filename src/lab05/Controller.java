/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lab05;

import java.lang.management.ThreadMXBean;

/**
 *
 * @author Matti
 */
public class Controller implements Runnable {

    ArrayListShelf alShelf;
    HashShelf hmShelf;
    TreeShelf tShelf;
    private int listSelector = 0;
    private long startTimeMs;
    private long taskTimeMs;

    public Controller(int selector) {
        alShelf = new ArrayListShelf();
        hmShelf = new HashShelf();
        tShelf = new TreeShelf();
        this.listSelector = selector;
    }

    @Override
    public void run() {
        startTimeMs = System.currentTimeMillis();
        for (int i = 0; i < 100000; i++) {
            CD cd = new CD();
            if (this.listSelector == 1) {
                alShelf.addToList(cd);

            } else if (this.listSelector == 2) {
                hmShelf.addToList(cd);
            } else {
                tShelf.addToList(cd);
            }

        }
        System.out.println("Adding of CDs for " + this.listSelector + " done.");
        orderLists();

    }

    private void orderLists() {
        if (this.listSelector == 1) {
            System.out.println("Starting sorting for ArrayList");
            alShelf.arrangeList();
            alShelf.find100th();
            System.out.println("Sorting finished for ArrayList.");
            this.taskTimeMs = System.currentTimeMillis() - this.startTimeMs;
            System.out.println("Time it took to add, sort & find for ArrayList " + this.taskTimeMs + "ms.");
        } else if (this.listSelector == 2) {
            System.out.println("Object ordering is not supported. for HashSet");
            hmShelf.find100th();
            this.taskTimeMs = System.currentTimeMillis() - this.startTimeMs;
            System.out.println("Time it took to add, & find for HashSEt " + this.taskTimeMs + "ms.");

        } else {
            tShelf.find100th();
            this.taskTimeMs = System.currentTimeMillis() - this.startTimeMs;
            System.out.println("Time it took to add, sort for TreeSet " + this.taskTimeMs + "ms.");
            System.out.println("Sorting finished for TreeSet.");
        }

    }

}
