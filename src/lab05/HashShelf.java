/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lab05;

import java.util.Collections;
import java.util.HashSet;

/**
 *
 * @author Matti
 */
public class HashShelf extends Shelf {

    HashSet<CD> hashList;

    public HashShelf() {
        this.hashList = new HashSet<>();
    }

    @Override
    public void addToList(CD cd) {
        this.hashList.add(cd);
    }

    @Override
    public void find100th() {
        int i = 0;
        for (CD cd : this.hashList) {
            i++;
            if (i % 10000 == 0) {
                System.out.println("HASHSET Found " + i + "th CD " + cd);
            }
        }
    }

    @Override
    public void arrangeList() {
        // Collections.sort(this.hashList);

    }

}
