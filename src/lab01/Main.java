/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lab01;
/**
 *
 * @author Matti
 */
import java.util.Scanner;

public class Main {
    public static void main(String args[]) {
        
        //doing everything in the correct order
        System.out.println("\nStarting the first run, everything is done correctly");
        WaterDispenserLab01 waterDispenser1 = new WaterDispenserLab01(100);
        waterDispenser1.dispenserOnOff();
        waterDispenser1.lidOnOff();
        waterDispenser1.addWater(30);
        waterDispenser1.lidOnOff();
        waterDispenser1.dispenseWater(10);
        
        //trying to dispense water without adding water first
        System.out.println("\nStarting the second run, trying to dispense without adding water");
        WaterDispenserLab01 waterDispenser2 = new WaterDispenserLab01(100);
        waterDispenser2.dispenserOnOff();
        waterDispenser2.dispenseWater(10);

        //trying to add water without opening the lid
        System.out.println("\nStarting the third run, trying to add water without opening the lid");
        WaterDispenserLab01 waterDispenser3 = new WaterDispenserLab01(100);
        waterDispenser3.dispenserOnOff();
        waterDispenser3.addWater(30);
        
        //trying to dispense more water than there is in the dispenser
        System.out.println("\nStarting the fourth run, trying to dispense more water than there is in the tank");
        WaterDispenserLab01 waterDispenser4 = new WaterDispenserLab01(30);
        waterDispenser4.dispenserOnOff();
        waterDispenser4.lidOnOff();
        waterDispenser4.addWater(30);
        waterDispenser4.lidOnOff();
        waterDispenser4.dispenseWater(40);

    }
      
    
}
