/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lab01;

/**
 *
 * @author Matti
 */
public class WaterDispenserLab01 {
    boolean power = false;
    boolean lidOn = true;
    int waterLevel = 0;
    int maxLevel = 10;
    
    public WaterDispenserLab01 () {
        System.out.println("Water dispenser created");
    }
    
    public WaterDispenserLab01 (int userLevel) {
        if (userLevel > 0) {
            this.maxLevel = userLevel;
            System.out.println("Water dispenser created with user defined maximum water level of " + userLevel + ".");
        }
        else {
            this.maxLevel = maxLevel;
            System.out.println("Water dispenser created with default maximum water level");
        }
        
    }
       
    public void addWater(int amount) {
        System.out.println("Water replenishment started...");
        if (!lidOn && amount > 0) {
            if (this.waterLevel + amount >= this.maxLevel) {
                this.waterLevel = this.maxLevel;
                System.out.println("The tank has been filled to the maximum water level allowed.");
            }
            else {
                this.waterLevel += amount;
                System.out.println("Water level is now at " + this.waterLevel);
            }
        }
        
        else if (this.lidOn) {
            System.out.println("The lid is on. Remove it first.");
        }
        
        else {
            System.out.println("Input a number > 0");
        }
    }
    
    public void dispenserOnOff() {
        this.power = !this.power;
        System.out.println("Is the system on? " + this.power);
    }
    
    public void lidOnOff() {
        if (this.power) {
            this.lidOn = !this.lidOn;
            System.out.println("Is the lid on? " + this.lidOn);
        }
        
        else {
            System.out.println("Turn on the machine first!");
        }
    }
    
    public void dispenseWater(int amount) {
        System.out.println("Trying to dispense water...");
        if (amount > 0) {
            if (this.power) {
                if (this.waterLevel >= amount) {
                    this.waterLevel -= amount;
                    System.out.println("Dispensing " + amount + " amounts of water!");
                    System.out.println("There is still " + this.waterLevel + " amounts of water left to drink.");
                }
                else if (this.waterLevel < amount && this.waterLevel > 0) {
                    System.out.println("The dispenser doesn't have the requested amount of water. Dispensing " + this.waterLevel + " amount(s) of water.");
                    this.waterLevel = 0;
                }
                else {
                    System.out.println("There is not enough water in the dispenser, please fill it.");
                }
            }

            else {
                System.out.println("Turn on the power!");
            }
        }
        else {
            System.out.println("The amount cannot be negative");
        }
    }
}
