/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lab02;

/**
 *
 * @author Matti
 */
public class CoffeeMaker {

    private int waterAmount = 0;
    private int beansAmount = 0;
    private int maxWater = 1000;
    private int maxBeans = 100;
    private int numberOfUses = 0;
    private int cleaningLimit = 5;
    private int nextCleaning = 0;
    private boolean cleaningRequired = false;
    private boolean power = false;
    private boolean rinse = false;

    public CoffeeMaker() {

    }

    public void addWater(int amount) {
        System.out.println("\nAdding water...");
        if (amount + this.waterAmount <= this.maxWater) {
            this.waterAmount += amount;
            System.out.println("Added " + amount + "ml of water and the water level is now at " + this.waterAmount + "ml.");
        }
        
        else {
            this.waterAmount = this.maxWater;
            System.out.println("Water tank has been filled and is now at " + maxWater + "ml.");
        }
    }
    
    public void addBeans(int amount) {
        System.out.println("\nAdding beans...");
        if (amount + this.beansAmount <= this.maxBeans) {
            this.beansAmount += amount;
            System.out.println("Added " + amount + "g of beans and the bean level is now at " + this.beansAmount + "g.");
        }
        
        else {
            this.beansAmount = this.maxBeans;
            System.out.println("Bean tank has been filled and is now at " + maxBeans + "g.");
        }

    }

    public void powerOnOff() {
        this.power = !this.power;
        System.out.println("Is the power on? " + this.power + ".");
    }

    public void rinse() {
        System.out.println("\nPreforming automated rinsing activity...");
        if (this.waterAmount >= 25 && this.power == true) {
            System.out.println("Rinsing completed. 25ml of water used.");
            System.out.println("A clean machine is a happy machine!");
            this.waterAmount -= 25;
            this.rinse = true;

        } 
        else {
            System.out.println("Could not preform rinsing. Add water and try again!");
            this.rinse = false;
        }
    }
    
    public void makeCoffee() {
        System.out.println("\nStarting coffee creating process...");
        if (this.waterAmount >= 150 && this.rinse == true && this.beansAmount >= 10 && this.cleaningRequired == false && this.power == true) {
            this.waterAmount -= 150;
            this.beansAmount -= 10;
            System.out.println("Your coffee is now in the mug or the on the floor. Enjoy!");
            cleaningCalc(1);
            
        }
        
        else if (this.rinse == false ||this.cleaningRequired == true) {
            System.out.println("The machine is not clean! Remember to clean it!");
            
        }
        
        else if (this.power != true) {
            System.out.println("Turn on the power first!");
        }
        
        else {
            System.out.println("There is only " + this.waterAmount + "ml of water and " + this.beansAmount + "g of beans.");
            System.out.println("The required amounts are 150 ml of water and 10g of beans.");
        }
    }

     public void makeEspresso() {
        System.out.println("\nStarting espresso creating process...");
        if (this.waterAmount >= 30 && this.rinse == true && this.beansAmount >= 8 && this.cleaningRequired == false && this.power == true) {
            this.waterAmount -= 150;
            this.beansAmount -= 10;
            System.out.println("Enjoy your ESPRESSO!");
            cleaningCalc(1);
            
        }
        
        else if (this.rinse == false ||this.cleaningRequired == true) {
            System.out.println("The machine is not clean! Remember to clean it!");
            
        }
        
        else if (this.power != true) {
            System.out.println("Turn on the power first!");
        }        
        
        else {
            System.out.println("There is only " + this.waterAmount + "ml of water and " + this.beansAmount + "g of beans.");
            System.out.println("The required amounts are 30 ml of water and 8g of beans.");
        }
    }   
    
    public void cleaningCalc(int use) {
        this.numberOfUses += use;
            System.out.println("\nThe machine has now been used " + this.numberOfUses + " times since the last cleaning.");
            if (this.numberOfUses >= cleaningLimit) {
                System.out.println("CLEAN THE MACHINE BEFORE USING IT ANY FURTHER!");
                this.cleaningRequired = true;
            }
            
            else {
                this.nextCleaning = this.cleaningLimit - this.numberOfUses;
                System.out.println("Uses left until next cleaning " + this.nextCleaning + ".");
            }
    }
    
    public void cleaningAction() {
        System.out.println("\nStarting cleaning sequence...");
        if (this.waterAmount >= 300  && this.power == true) {
            this.cleaningRequired = false;
            this.numberOfUses = 0;
            this.nextCleaning = 5;
            System.out.println("Cleaning completed! You can use the machine for " + this.nextCleaning + " times.");
        }
        
        else if (this.power != true) {
            System.out.println("Turn on the power first!");
        }        
               
        else {
            System.out.println("There is only " + this.waterAmount + "ml of water and for cleaning it's required to have 300ml");
            System.out.println("Not enough water to preform cleaning... add more water!");
        }
    }
}
