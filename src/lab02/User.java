/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lab02;

/**
 *
 * @author Matti
 */
public class User {
    public static void main(String args[]) {
        CoffeeMaker coffeeMaker = new CoffeeMaker();
        coffeeMaker.addWater(500);
        coffeeMaker.addBeans(100);
        coffeeMaker.powerOnOff();
        coffeeMaker.rinse();
        coffeeMaker.makeCoffee();
        coffeeMaker.makeCoffee();
        coffeeMaker.makeCoffee();
        coffeeMaker.addWater(500);
        coffeeMaker.addBeans(100);
        coffeeMaker.makeEspresso();
        coffeeMaker.makeEspresso();
        coffeeMaker.makeEspresso();
        coffeeMaker.addWater(100);
        coffeeMaker.cleaningAction();
        coffeeMaker.makeEspresso();
        coffeeMaker.rinse();
        coffeeMaker.powerOnOff();
        coffeeMaker.makeEspresso();
    }
}
