/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lab03;

/**
 *
 * @author Matti
 */
public class TrackPlayer implements Runnable {
    private int trackNumber = 0;
    private String trackName = "";
    private CD cd;
    private int totalTrackLenght = 0;
    private int trackLenghtSec = 0;
    private int trackLenghtMin = 0;
    
    public TrackPlayer(int trackNumber, CD cd) {
        System.out.println("Ready to play a track!");
        this.trackNumber = trackNumber;
        this.cd = cd;
        getTrackInfo();
    }

    @Override
    public void run() {
        
    }

    private void getTrackInfo() {
        System.out.println("Step 5");
        this.trackName = cd.getTrackName(trackNumber);
        System.out.println("Playing  the song " + this.trackName + ".");
    }
    
    
}
