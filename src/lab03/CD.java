/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lab03;
import java.util.ArrayList;
/**
 *
 * @author Matti
 */
public class CD {
    private String cdName = "";
    private String trackName = "";
    private ArrayList <Track> trackList = new ArrayList();
    private Track track;
    
    public CD (String name, int amountOfTracks) {
        this.cdName = name;
        System.out.println("New CD " + this.cdName + " created.");
    }
    
    public void addTracksToCD(Track track) {
        this.trackList.add(track);
        this.trackName = track.getTrackName();
        System.out.println("Added track " + this.trackName + " to CD.");
    }
    
    public String getCDName() {
        return this.cdName;
    }
    public void getTrackListing() {
        for (Track o : this.trackList) {
            System.out.println(o.getTrackName());
        }
    }
    
    public String getTrackName(int indexNumber) {
        try {
            System.out.println("step 6");
            track = this.trackList.get(indexNumber);
            this.trackName = track.getTrackName();
            System.out.println("step 7");
            return this.trackName;
        }
        
        catch (Exception e) {
            System.out.println("No such track found.");
        }
        
        return this.trackName;
    }
    
    public void getTrackLenght() {
        
    }
    
}
