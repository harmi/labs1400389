/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lab03;

import java.util.ArrayList;

/**
 *
 * @author Matti
 */
public class Player {

    private boolean power = false;
    private ArrayList<CD> cdList = new ArrayList<CD>();
    private int currentTrack = 0;
    private CD currentCD;
    private int volume = 50;
    private TrackPlayer trackPlayer;
    private String trackName = "Asd";
    Thread t;

    public Player(int cdAmount) {
        System.out.println("New CD player created!");
    }

    public void togglePower() {
        if (this.power) {
            this.power = false;
            System.out.println("Power off.");
        } else {
            this.power = true;
            System.out.println("Power on.");
        }
    }

    public void playButton() {
        System.out.println("Step 2");
        if (!this.power) {
            if (this.currentCD != null) {
                //int trackNumber, String trackName, CD cd, int trackLenght
                System.out.println("Step 333");
                this.trackPlayer = new TrackPlayer(currentTrack, currentCD);
                t = new Thread(trackPlayer);
                

            } else {
                System.out.println("Insert CD!");
            }

        } else {
            System.out.println("Turn on the power first!");
        }

    }

    public void insertCD(CD cd) {
        if (this.cdList.isEmpty() && cd != null) {
            this.cdList.add(cd);
            System.out.println("Added CD to the CD tray. " + cd);
            this.currentCD = cd;
        } else {
            System.out.println("Not a valid CD.");
        }
    }

    public void eject() {
        if (this.cdList.isEmpty()) {
            System.out.println("Cannot eject CD, nothing has been inserted.");
        } else {
            this.cdList.remove(0);
            System.out.println("CD has been ejected.");
        }
    }

    public void trackListing() {
        if (this.cdList.isEmpty()) {
            System.out.println("No CD in the tray!");
        } else {
            for (CD o : this.cdList) {
                o.getTrackListing();
            }
        }
    }

    public void setVolume(int volume) {
        this.volume = volume;
        System.out.println("Volume set to " + this.volume);
    }

}
