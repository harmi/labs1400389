/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lab03;

import java.util.HashMap;
import java.util.Scanner;

/**
 *
 * @author Matti
 */
public class User {

    HashMap<String, CD> listOfAllCDs;
    // creating a list that holds all of the CDs
    Player player;
    //creating a CD with name and 8 tracks
    CD cd = new CD("Make it big", 8);
    //creating the tracs, name and lenght in seconds
    //It's Wham!s second album, featuring the song of "Careless whisper"
    Track firstTrack = new Track("Wake me up before you go-go", 230);
    Track secondTrack = new Track("Everything she wants", 301);
    Track thirdTrack = new Track("Heartbeat", 282);
    Track fourthTrack = new Track("Like a baby", 252);
    Track fifthTrack = new Track("Freedom", 305);
    Track sixthTrack = new Track("If you were there", 218);
    Track seventhTrack = new Track("Credit card baby", 308);
    Track eightTrack = new Track("Careless whisper", 390);

    Scanner reader = new Scanner(System.in);
    private boolean loop = true;
    private int selection = 0;
    private String cdSelection = "";
    private String cdLoop = "";
    private int volumeSelection = 0;

    public User() {
        this.player = new Player(1);

        //adding the tracks to the CD
        cd.addTracksToCD(firstTrack);
        cd.addTracksToCD(secondTrack);
        cd.addTracksToCD(thirdTrack);
        cd.addTracksToCD(fourthTrack);
        cd.addTracksToCD(fifthTrack);
        cd.addTracksToCD(sixthTrack);
        cd.addTracksToCD(seventhTrack);
        cd.addTracksToCD(eightTrack);
        listOfAllCDs = new HashMap();
        listOfAllCDs.put("Make it big", cd);

    }

    public void userLoop() {
        while (loop) {
            //add mute
            System.out.println("\n MAIN MENU");
            System.out.println("1. Play/Pause \n2. Stop \n3. Next \n4. Previous");
            System.out.println("5. Suffle \n6. Repeat \n7. Volume \n8. List of songs");
            System.out.println("9. Insert CD \n10. Eject CD \n11. Power On/Off \n12. Exit");
            System.out.print("Your selection: ");
            try {
                selection = Integer.parseInt(reader.nextLine());
                switch (selection) {
                    case 1:
                        player.playButton();
                        System.out.println("Step 1");
                        break;
                    case 2:
                        player.playButton();
                    case 3:
                        player.playButton();
                    case 4:
                        player.playButton();
                    case 5:
                        player.playButton();
                    case 6:
                        player.playButton();
                    case 7:
                        selection = pollVolume();
                        player.setVolume(selection);
                        break;
                    case 8:
                        player.trackListing();
                        break;
                    case 9:
                        chooseCD();
                        break;
                    case 10:
                        player.eject();
                        break;
                    case 11:
                        player.togglePower();
                        break;
                    case 12:
                        System.out.println("Bye bye!");
                        loop = false;
                        break;
                    default:
                        System.out.println("Choose something between 1-12.");
                }
            } catch (Exception e) {
                System.out.println("Numbers only!");
            }
        }

    }

    public void chooseCD() {
        System.out.println("\nList of possible CDs: ");
        System.out.println(listOfAllCDs.keySet());

        System.out.print("What CD do you want to insert? (Type the name, case sensitive) ");
        cdSelection = reader.nextLine();
        try {
            player.insertCD(listOfAllCDs.get(cdSelection));

        } catch (Exception e) {
        }
    }

    public int pollVolume() {
        while (true) {
            System.out.println("Select volume level 0-100 ");
            try {
                volumeSelection = Integer.parseInt(reader.nextLine());
                if (volumeSelection < 100 && volumeSelection > 0) {
                    break;
                } else {
                    System.out.println("Input a number between 0-100!");
                }

            } catch (Exception e) {
                System.out.println("Numbers only!");
            }
        }
        return volumeSelection;
    }
}
